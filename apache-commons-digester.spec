%global short_name commons-digester
Name:                apache-%{short_name}
Version:             2.1
Release:             1
Summary:             XML to Java object mapping module
License:             ASL 2.0
URL:                 http://commons.apache.org/digester/
BuildArch:           noarch
Source0:             http://archive.apache.org/dist/commons/digester/source/commons-digester-%{version}-src.tar.gz
BuildRequires:       maven-local mvn(commons-beanutils:commons-beanutils)
BuildRequires:       mvn(commons-logging:commons-logging) mvn(junit:junit)
BuildRequires:       mvn(org.apache.commons:commons-parent:pom:)
%description
Many projects read XML configuration files to provide initialization of
various Java objects within the system. There are several ways of doing this,
and the Digester component was designed to provide a common implementation
that can be used in many different projects

%package help
Summary:             API documentation for %{name}
Provides:            %{name}-javadoc = %{version}-%{release}
Obsoletes:           %{name}-javadoc < %{version}-%{release}
%description help
This package contains the %{summary}.

%prep
%setup -q -n %{short_name}-%{version}-src
%mvn_alias "%{short_name}:%{short_name}" "org.apache.commons:%{short_name}"
%mvn_file :%{short_name} %{short_name} %{name}

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt NOTICE.txt RELEASE-NOTES.txt

%files help -f .mfiles-javadoc
%doc LICENSE.txt NOTICE.txt

%changelog
* Sat Aug 22 2020 liyanan <liyanan32@huawei.com> - 2.1-1
- package init
